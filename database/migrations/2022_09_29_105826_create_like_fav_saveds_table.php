<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_fav_saveds', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('like_id');
            $table->foreign('like_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('save_id');
            $table->foreign('save_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('fav_id');
            $table->foreign('fav_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('dislike_id');
            $table->foreign('dislike_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_fav_saveds');
    }
};
