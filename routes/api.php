<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('auth/register', [AuthController::class, 'register']);
Route::post('auth/login', [AuthController::class, 'login']);

Route::group(["namespace"=>"App\Http\Controllers\Api", 'middleware'=>'auth:sanctum'], function(){
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::ApiResource('/userinfo', UserInfoController::class);
});



// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/matches', 'MatchesController@matches')->name('matches');

// Route::get('/pictures', 'UserPicturesController@show')->name('pictures.show');
// Route::post('/pictures', 'UserPicturesController@addPictures')->name('pictures.add');
// Route::delete('/pictures/{id}', 'UserPicturesController@destroyPicture')->name('pictures.destroy');

// Route::get('/profile/edit', 'EditUserProfileController@show')->name('profile.showEditProfile');
// Route::post('/profile/edit', 'EditUserProfileController@updateProfile')->name('profile.updateProfile');
// Route::put('/profile/edit', 'EditUserProfileController@updateProfilePicture')->name('profile.updateProfilePicture');

// Route::get('/profile/settings', 'EditUserProfileController@showSettings')->name('profile.showSettings');
// Route::put('/profile/settings', 'EditUserProfileController@updateSettings')->name('profile.updateSettings');

// Route::delete('/profile', 'EditUserProfileController@destroyProfile')->name('profile.destroy');
// Route::post('/profile/like/{id}', 'ReactionController@like')->name('like');
// Route::post('/profile/dislike/{id}', 'ReactionController@dislike')->name('dislike');
