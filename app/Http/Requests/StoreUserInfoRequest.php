<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUserInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'education' => ['required'],
            'career' => ['required'],
            'languages' => ['required'],
            'age' => ['required'],
            'gender' => ['required'],
            'profile_picture' => ['required'],
            'expectations_tags' => ['required'],
        ];
    }
    protected function prepareForValidation(){
        $this->merge([
            'profile_picture' => $this->profilePicture,
            'expectations_tags' => $this->expectationTags
        ]);
    }
}
