<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLikeFavSavedRequest;
use App\Http\Requests\UpdateLikeFavSavedRequest;
use App\Models\LikeFavSaved;

class LikeFavSavedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreLikeFavSavedRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLikeFavSavedRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LikeFavSaved  $likeFavSaved
     * @return \Illuminate\Http\Response
     */
    public function show(LikeFavSaved $likeFavSaved)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LikeFavSaved  $likeFavSaved
     * @return \Illuminate\Http\Response
     */
    public function edit(LikeFavSaved $likeFavSaved)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateLikeFavSavedRequest  $request
     * @param  \App\Models\LikeFavSaved  $likeFavSaved
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLikeFavSavedRequest $request, LikeFavSaved $likeFavSaved)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LikeFavSaved  $likeFavSaved
     * @return \Illuminate\Http\Response
     */
    public function destroy(LikeFavSaved $likeFavSaved)
    {
        //
    }
}
