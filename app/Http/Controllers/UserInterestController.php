<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserInterestRequest;
use App\Http\Requests\UpdateUserInterestRequest;
use App\Models\UserInterest;

class UserInterestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUserInterestRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserInterestRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserInterest  $userInterest
     * @return \Illuminate\Http\Response
     */
    public function show(UserInterest $userInterest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserInterest  $userInterest
     * @return \Illuminate\Http\Response
     */
    public function edit(UserInterest $userInterest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserInterestRequest  $request
     * @param  \App\Models\UserInterest  $userInterest
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserInterestRequest $request, UserInterest $userInterest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserInterest  $userInterest
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserInterest $userInterest)
    {
        //
    }
}
