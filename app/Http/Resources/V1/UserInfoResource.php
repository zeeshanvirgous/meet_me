<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class UserInfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'education' => $this->education,
            'career' => $this->career,
            'langauges' => $this->languages,
            'age' => $this->age,
            'gender' => $this->gender,
            'profilePicture' => $this->profile_picture,
            'expectationsTags' => $this->expectations_tags,
        ];
    }



}
