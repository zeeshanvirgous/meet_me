<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{


    use Notifiable, HasApiTokens;

    protected $fillable = [
        'name',
        'email',
        'password',
        'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function info()
    {
        return $this->hasOne('App\Models\UserInfo');
    }

    public function settings()
    {
        return $this->hasOne('App\Models\UserSettings');
    }

    public function pictures()
    {
        return $this->hasMany('App\Models\Picture');
    }

    public function userLiked()
    {
        return $this->belongsToMany('App\Models\User', 'matches', 'user_two', 'user_one');
    }

    public function likedUser()
    {
        return $this->belongsToMany('App\Models\User', 'matches', 'user_one', 'user_two');
    }

    public function dislikes()
    {
        return $this->belongsToMany('App\Models\User', 'dislikes', 'user_two', 'user_one');
    }
}
