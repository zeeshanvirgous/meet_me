<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class UserInfo extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'education',
        'career',
        'language',
        'age',
        'gender',
        'profile_picture',
        'expectations_tags',
        'languages'
    ];

    public function getPicture()
    {
        if($this->profile_picture == null)
        {
            return Storage::url('/picture/default.png');
        }
        return Storage::url($this->profile_picture);
    }

    public function user()
    {
        return $this->BelongsTo('App\Models\User');
    }

    public function match(User $foreignUser)
    {
        $thisMatched = $this->belongsToMany('App\User', 'matches',
            'user_one', 'user_two')
            ->where('user_one', '=', $this->id)
            ->where('user_two', '=', $foreignUser->id)->getResults();

        $matchedThis = $this->belongsToMany('App\User', 'matches',
            'user_two', 'user_one')
            ->where('user_two', '=', $this->id)
            ->where('user_one', '=', $foreignUser->id)->getResults();

        if (isset($thisMatched[0]->attributes['id']) && isset($matchedThis[0]->attributes['id'])) {
            return $foreignUser;
        } else {
            return null;
        }
    }

    public function scopeSearchWithSettings($query, $from, $to, $gender, $id)
    {
        if ($gender == 'both') {
            return $query->whereHas('info', function ($query) use ($from, $to, $id) {
                $query->where('age', '>=', $from)
                    ->where('age', '<=', $to)
                    ->where('user_id', '!=', $id)
                    ->where('profile_picture', '!=', '');
            });
        } else {
            return $query->whereHas('info', function ($query) use ($from, $to, $gender, $id) {
                $query->where('age', '>=', $from)
                    ->where('age', '<=', $to)
                    ->where('gender', $gender)
                    ->where('user_id', '!=', $id)
                    ->where('profile_picture', '!=', '');
            });
        }
    }

    public function scopeSearchWithoutLikesAndDislikes($query, $id)
    {
        return $query->whereDoesntHave('userLiked', function ($query) use ($id) {
            $query->where('user_one', $id);
        })
            ->whereDoesntHave('dislikes', function ($query) use ($id) {
                $query->where('user_one', $id);
            });
    }

    public function scopeSearchMatches($query, $id)
    {
        return $query->whereHas('userLiked', function ($query) use ($id) {
            $query->where('user_one', $id);
        })
            ->whereHas('likedUser', function ($query) use ($id) {
                $query->where('user_two', $id);
            })
            ->whereDoesntHave('dislikes', function ($query) use ($id) {
                $query->where('user_one', $id);
            });
    }

    public function scopeSearchLikes($query, $id)
    {
        return $query->whereHas('userLiked', function ($query) use ($id) {
            $query->where('user_one', $id);
        })
            ->whereDoesntHave('likedUser', function ($query) use ($id) {
                $query->where('user_two', $id);
            })
            ->whereDoesntHave('dislikes', function ($query) use ($id) {
                $query->where('user_one', $id);
            });
    }
}
